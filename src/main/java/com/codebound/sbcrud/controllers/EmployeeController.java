package com.codebound.sbcrud.controllers;

import com.codebound.sbcrud.models.Employee;
import com.codebound.sbcrud.repos.EmployeeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EmployeeController {

    // dependency injection
    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    // Methods
    // get all employees and display them on employees/index view

    @GetMapping("/employees")
    public String showEmployees(Model model) {
        List<Employee> employeeList = employeeRepository.findAll();

        model.addAttribute("noEmployeesFound", employeeList.size() == 0);
        model.addAttribute("employees", employeeList);

        return "employees/index";
    }

    // add an employee form
    @GetMapping("/employees/create")
    public String showEmployee(Model model) {
        model.addAttribute("newEmployee", new Employee());

        return "employees/create";
    }

    @PostMapping("/employees/create")
    public String createEmployee(@ModelAttribute Employee employeeToCreate) {
        // save employeeToCreate parameter
        employeeRepository.save(employeeToCreate);

        // redirect user to the list of employees
        return "redirect:/employees";
    }

    // method to display individual employee
    @GetMapping("/employees/{id}")
    public String showEmployee(@PathVariable long id, Model model) {
        Employee employee = employeeRepository.getOne(id); // getting employee by id

        model.addAttribute("showEmployee", employee);

        return "employees/show";
    }

    // method to allow user to edit employee info
    @GetMapping("/employees/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        // find an employee with the passed in id
        Employee employeeToEdit = employeeRepository.getOne(id);
        model.addAttribute("editEmployee", employeeToEdit);

        return "employees/edit";
    }

    // method to process the edited info
    @PostMapping("/employees/{id}/edit")
    public String updateEmployee(@PathVariable long id,
                                 @RequestParam(name = "firstName")String firstName,
                                 @RequestParam(name = "lastName")String lastName,
                                 @RequestParam(name = "employeeNumber")String employeeNumber,
                                 @RequestParam(name = "userName")String userName,
                                 @RequestParam(name = "email")String email)
    {
        // find the employee with the passed in id
        Employee foundEmployee = employeeRepository.getEmployeeById(id);

        // update the employee's info
        foundEmployee.setFirstName(firstName);
        foundEmployee.setLastName(lastName);
        foundEmployee.setEmployeeNumber(employeeNumber);
        foundEmployee.setUserName(userName);
        foundEmployee.setEmail(email);

        // save the new employee's data changes
        employeeRepository.save(foundEmployee);

        // redirect the user to the url that contains the list of employees
        return "redirect:/employees/";
    }

    // method to allow user to delete employees
    @PostMapping("/employees/{id}/delete")
    public String delete(@PathVariable long id) {
        // access the deleteById() from the repository
        employeeRepository.deleteById(id);

        // redirect to the url with the list employees
        return "redirect:/employees/";
    }

}





















