package com.codebound.sbcrud.repos;

import com.codebound.sbcrud.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query("from Employee a where a.id like?1")
    Employee getEmployeeById(long id);
}
